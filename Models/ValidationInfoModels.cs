﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BarCodeStorage.Models
{
    public class ManualTakeBloodInfoModel
    {
        public string Blood_No { get; set; }//血液条码
        public string Eia_No { get; set; }//酶免条码
        public string Nat_No { get; set; }//核酸条码
        public string Table_No { get; set; }//表格条码
        public string Eq_Id { get; set; }//设备ID
        public string Braid_No { get; set; }//辫子条码
        public string Check_Result { get; set; }//核对结果
        public string Check_Person { get; set; }//核对人
        public string version { get; set; }//版本号
        public string Remark { get; set; }//备注
        public DateTime Check_Date { get; set; }//核对时间
    }

    public class IngredientInfoModel
    {
        public string Eq_Id { get; set; } //设备ID
        public string Blood_No1 { get; set; } //血液条码1
        public string Blood_No2 { get; set; } //血液条码2
        public string Blood_No3 { get; set; } //血液条码3
        public string Blood_No4 { get; set; } //血液条码4
        public string Check_Result { get; set; } //核对结果
        public string Check_Person { get; set; } //核对人
        public DateTime Check_Date { get; set; } //核对时间
        public string version { get; set; } //版本号
        public string Remark { get; set; } //备注

    }

    public class MechanicalTakeBloodModel
    {
        public string Eq_Id { get; set; }//设备ID
        public string Blood_No1 { get; set; }//血液条码
        public string Blood_No2 { get; set; }//血液条码
        public string Eia_No { get; set; }//酶免条码
        public string Nat_No { get; set; }//核酸条码
        public string Table_No { get; set; }//表格条码
        public string Braid_No { get; set; }//辫子条码
        public string Sample_No { get; set; }//样品条码
        public string Check_Result { get; set; }//核对结果
        public string Check_Person { get; set; }//核对人
        public DateTime Check_Date { get; set; }//核对时间
        public string version { get; set; }//版本号
        public string Remark { get; set; }//备注

    }
}