﻿using System;
using BarCodeStorage.Models;

namespace DataAccess
{
    class BarCodeDataAccess :DataAccessBase, IBarCodeDataAccess
    {
        public bool InsertManualTakeBloodInfo(ManualTakeBloodInfoModel info)
        {
            string sqlStr = $"insert into tc_code_check_info " +
                            $"(FID,Blood_No,Eia_No,Nat_No,Table_No,Eq_Id,Braid_No,Check_Result,Check_Person,version,Remark,Check_Date) values " +
                            $"('{Guid.NewGuid()}','{info.Blood_No}','{info.Eia_No}','{info.Nat_No}','{info.Table_No}','{info.Eq_Id}','{info.Braid_No}','{info.Check_Result}','{info.Check_Person}','{info.version}','{info.Remark}','{info.Check_Date}')";
            Execute(sqlStr);
            return true;
        }

        public bool InsertIngredientInfo(IngredientInfoModel info)
        {
            string sqlStr = $"insert into cf_code_check_info " +
                            $"(FID,Eq_Id,Blood_No1,Blood_No2,Blood_No3,Blood_No4,Check_Result,Check_Person,Check_Date,version,Remark) values " +
                            $"('{Guid.NewGuid()}','{info.Eq_Id}','{info.Blood_No1}','{info.Blood_No2}','{info.Blood_No3}','{info.Blood_No4}','{info.Check_Result}','{info.Check_Person}','{info.Check_Date}','{info.version}','{info.Remark}')";
            Execute(sqlStr);
            return true;
        }

        public bool InsertMechanicalTakeBlood(MechanicalTakeBloodModel info)
        {
            string sqlStr = $"insert into dc_code_check_info " +
                            $"(FID,Eq_Id,Blood_No1,Blood_No2,Eia_No,Nat_No,Table_No,Braid_No,Sample_No,Check_Result,Check_Person,Check_Date,version,Remark) values " +
                            $"('{Guid.NewGuid()}','{info.Eq_Id}','{info.Blood_No1}','{info.Blood_No2}','{info.Eia_No}','{info.Nat_No}','{info.Table_No}','{info.Braid_No}','{info.Sample_No}','{info.Check_Result}','{info.Check_Person}','{info.Check_Date}','{info.version}','{info.Remark}')";
            Execute(sqlStr);
            return true;
        }
    }
}