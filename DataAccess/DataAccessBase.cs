﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDMK.Utility;

namespace DataAccess
{
    class DataAccessBase
    {
        public DataAccessBase()
        {
            OracleConn = new OracleConnection();
            OracleConn.ConnectionString = ConfigHelper.GetString("OracleConnection");
            OracleConn.Open();
        }

        protected OracleConnection OracleConn;


        public DataSet Execute(string sqlStr)
        {
            var adapter=new OracleDataAdapter(sqlStr,OracleConn);
            DataSet dtTmp = new DataSet();
            adapter.Fill(dtTmp);
            return dtTmp;
        }
    }
}
