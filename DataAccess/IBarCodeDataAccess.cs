﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BarCodeStorage.Models;

namespace DataAccess
{
    public interface IBarCodeDataAccess
    {
        bool InsertManualTakeBloodInfo(ManualTakeBloodInfoModel info);
        bool InsertIngredientInfo(IngredientInfoModel info);
        bool InsertMechanicalTakeBlood(MechanicalTakeBloodModel info);
    }
}
