﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BarCodeStorage.Models;
using NLog;

namespace BarCodeStorage.Controllers.APIs
{
    public partial class StoreMechanicalTakeBloodController : BaseApiController
    {

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] MechanicalTakeBloodModel value)
        {
            Logger.Log(LogLevel.Info,
                $"设备ID :{value.Eq_Id},血液条码 :{value.Blood_No1},血液条码 :{value.Blood_No2},酶免条码 :{value.Eia_No},核酸条码 :{value.Nat_No},表格条码 :{value.Table_No},辫子条码 :{value.Braid_No},样品条码 :{value.Sample_No},核对结果 :{value.Check_Result},核对人 :{value.Check_Person},核对时间 :{value.Check_Date},版本号 :{value.version},备注 :{value.Remark}");
            try
            {
                var result = DataAccess.DataAccessaManageFactory.CreateBarCodeDataAccess().InsertMechanicalTakeBlood(value);
                if (result)
                {
                    return ResponseSuccessfulMessage(true);
                }
                return ResponseFailedMessage();
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
                return ResponseExceptionMessage();
            }
            
        }

        
    }
}