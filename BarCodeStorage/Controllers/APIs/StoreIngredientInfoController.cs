﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BarCodeStorage.Models;
using NLog;

namespace BarCodeStorage.Controllers.APIs
{
    public class StoreIngredientInfoController : BaseApiController
    {

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] IngredientInfoModel value)
        {
            Logger.Log(LogLevel.Info,
                $"设备ID :{value.Eq_Id},血液条码1 :{value.Blood_No1},血液条码2 :{value.Blood_No2},血液条码3 :{value.Blood_No3},血液条码4 :{value.Blood_No4},核对结果 :{value.Check_Result},核对人 :{value.Check_Person},核对时间 :{value.Check_Date},版本号 :{value.version},备注 :{value.Remark}");
            try
            {
                var result = DataAccess.DataAccessaManageFactory.CreateBarCodeDataAccess().InsertIngredientInfo(value);
                if (result)
                {
                    return ResponseSuccessfulMessage(true);
                }
                return ResponseFailedMessage();
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
                return ResponseExceptionMessage();
            }
            
        }

        
    }
}