﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BarCodeStorage.Models;
using NLog;

namespace BarCodeStorage.Controllers.APIs
{
    public partial class StoreManualTakeBloodInfoController : BaseApiController
    {

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] ManualTakeBloodInfoModel value)
        {
            Logger.Log(LogLevel.Info,
                $"Eq_Id:{value.Eq_Id} Blood_No:{value.Blood_No} Eia_No:{value.Eia_No} Nat_No:{value.Nat_No} Table_No:{value.Table_No} Braid_No:{value.Braid_No} Check_Result:{value.Check_Result} Check_Person:{value.Check_Person} Check_Date:{value.Check_Date} version:{value.version} Remark:{value.Remark}");
            try
            {
                var result = DataAccess.DataAccessaManageFactory.CreateBarCodeDataAccess().InsertManualTakeBloodInfo(value);
                if (result)
                {
                    return ResponseSuccessfulMessage(true);
                }
                return ResponseFailedMessage();
            }
            catch (Exception e)
            {
                Logger.Log(LogLevel.Error, e);
                return ResponseExceptionMessage();
            }


        }

        
    }
}