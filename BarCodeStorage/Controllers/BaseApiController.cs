﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;

namespace BarCodeStorage.Controllers
{
    public class BaseApiController : ApiController
    {
        public BaseApiController()
        {
            Logger = LogManager.GetLogger(GetType().FullName);
        }

        protected Logger Logger { get; set; }
       
        protected HttpResponseMessage ResponseSuccessfulMessage(object result)
        {
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        protected HttpResponseMessage ResponseFailedMessage()
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "400:BadRequest");
        }

        protected HttpResponseMessage ResponseExceptionMessage()
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "500:InternalServerError");
        }

    }

    
}